import React,{Fragment} from 'react';
import 'antd/dist/antd.css';
import { Router, Switch, Route } from 'react-router-dom'

import {
    Header,
    ToDoList,
    Main_page
} from './views'

import history from './history'


function App() {
  return (
    <Fragment>
        <Header/>
        <Router history={history}>
            <Switch>
                <Route path='/user' component={ToDoList}/>
                <Route path='/' component={Main_page} />
                <Route path='*' render={() => <div>404</div>} />
            </Switch>
        </Router>
    </Fragment>
  );
}

export default App;
