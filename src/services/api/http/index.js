import axios from 'axios'

import {
  token,
  statusCode,
  connection
} from './incereptors'

import {baseURL} from '../../../constants'

const instance = axios.create({
  baseURL: baseURL.baseURL,
  headers: {},
  data: {}
});


token(instance);
statusCode(instance);
connection(instance);

export default instance
