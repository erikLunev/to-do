import http from './http'


const getToDos = data => {
  return http({
    url: '/toDo',
    method: 'get',
    data
  })
}

const createToDo = data => {
  return http({
    url: '/toDo',
    method: 'POST',
    data,
    headers: {
      'Content-type': 'application/json'
    }
  })
}

const removeToDo = id => {
  return http({
    url: `toDo/${id}`,
    method: 'DELETE'
  })
}

const updateToDo = (id, data) => {
  return http({
    url: `toDo/${id}`,
    method: 'PUT',
    data
  })
}

export {
  getToDos,
  createToDo,
  removeToDo,
  updateToDo
}
