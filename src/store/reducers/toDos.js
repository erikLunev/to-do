const initialState = {
  list: [],
  isFetching: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case "GET_TODOS_FETCHING":
      return {
        ...state,
        isFetching: action.payload
      }
    case "GET_TODOS_SUCCESS":
      return {
        ...state,
        list: action.payload
      }
    case "CREATE_TODO_FETCHING":
      return {
        ...state,
        isFetching: action.payload
      }
    case "CREATE_TODO_SUCCESS":
      return {
        ...state,
        list: [
          ...state.list,
          action.payload
        ]
      }
    case "REMOVE_TODO_BY_ID_FETCHING":
      return {
        ...state,
        isFetching: action.payload
      }
    case "REMOVE_TODO_BY_ID_SUCCESS": {
      return {
        ...state,
        list: state.list.filter(TODO => TODO._id !== action.payload)
      }
    }
    case "UPDATE_TODO_BY_ID_FETCHING": {
      return {
        ...state,
        isFetching: action.payload
      }
    }
    case "UPDATE_TODO_BY_ID_SUCCESS": {
      return {
        ...state,
        list: state.list.map(TODO =>
          TODO._id === action.payload.id
            ? { ...TODO, ...action.payload }
            : TODO)
      }
    }
    default:
      return state
  }
}
