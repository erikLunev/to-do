import {combineReducers} from "redux";
import toDos from './toDos'
import auth from './auth'

export default combineReducers({
    toDos,
    auth
})