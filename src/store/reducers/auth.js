const initialState = {
    isLogged: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case "LOGIN_USER":
            return {
                ...state,
                isLogged: action.payload
            }
        default:
            return state
    }
}
