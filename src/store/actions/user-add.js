// import http from '../../services/api/http'
import {message} from "antd";

const userPostFetch = user => {
    return dispatch => {
        return fetch(" https://lab.dev.cogniteq.com/api/auth/signUp", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
            },
            body: JSON.stringify(user)
        })
            .then(resp => resp.json())
            .then(data => {
                if (data.message) {
                    message.info(data.message);
                } else if (data.error){
                    message.warning(data.error);
                }
            })
            .catch(() => message.error('something went wrong'))
        }
    }


export default userPostFetch