import {api} from "../../services"
import {message} from 'antd'

const getToDos = () => dispatch => {
  dispatch({
    type: "GET_TODOS_FETCHING",
    payload: true
  })
  return api.toDos.getToDos()
    .then((data) => {
      if (data) {
        dispatch({
          type: "GET_TODOS_SUCCESS",
          payload: data
        })
      }
      dispatch({
        type: "GET_TODOS_FETCHING",
        payload: false
      })
    })
    .catch(err => message.error('something went wrong'))
}

const createToDo = (data) => dispatch => {
  dispatch({
    type: "CREATE_TODO_FETCHING",
    payload: true
  })
  return api.toDos.createToDo(data)
    .then((data) => {
      if (data) {
        message.info('Todo successfully created')
        dispatch({
          type: "CREATE_TODO_SUCCESS",
          payload: data
        })
      }
      if (data.error) {
        message.warning(data.error)
      }
      dispatch({
        type: "CREATE_TODO_FETCHING",
        payload: false
      })
    })
    .catch(err => message.error('something went wrong'))
}

const removeToDoById = (id) => dispatch => {
  dispatch({
    type: "REMOVE_TODO_BY_ID_FETCHING",
    payload: true
  })
  return api.toDos.removeToDo(id)
    .then((data ) => {
      if (data.message) {
        message.info(data.message)
        dispatch({
          type: "REMOVE_TODO_BY_ID_SUCCESS",
          payload: id
        })
      }
      if (data.error) {
        message.warning(data.error)
      }
      dispatch({
        type: "REMOVE_TODO_BY_ID_FETCHING",
        payload: false
      })
    })
    .catch(err => message.error('something went wrong'))
}

const updateToDoById = (id, todo) => dispatch => {
  dispatch({
    type: "UPDATE_TODO_BY_ID_FETCHING",
    payload: true
  })
  return api.toDos.updateToDo(id, todo)
    .then(( data ) => {
      if (data.message) {
        message.info(data.message)
        dispatch({
          type: "UPDATE_TODO_BY_ID_SUCCESS",
          payload: { id, ...todo }
        })
      }
      if (data.error) {
        message.warning(data.error)
      }
      dispatch({
        type: "UPDATE_TODO_BY_ID_FETCHING",
        payload: false
      })
    })
    .catch(err => message.error('something went wrong'))
}

export {
  getToDos,
  createToDo,
  updateToDoById,
  removeToDoById
}
