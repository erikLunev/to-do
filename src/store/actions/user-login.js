import history from "../../history";
import {message} from "antd";

const userLoginFetch = user => {
    return dispatch => {
        return fetch("https://lab.dev.cogniteq.com/api/auth/signIn", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json',
            },
            body: JSON.stringify(user)
        })
            .then(resp => resp.json())
            .then(data => {
                if (data.token) {
                    localStorage.setItem('token', data.token)
                    dispatch({
                        type: "LOGIN_USER",
                        payload: true
                    })
                    history.push('/user')
                } else if (data.error) {
                    message.error(data.error)
                }
            })
            .catch(err => message.error('something went wrong'))
    }
}

export default userLoginFetch