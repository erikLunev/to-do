import * as toDos from './toDos'
import * as auth from './user-login'

export default {
    toDos,
    auth
}