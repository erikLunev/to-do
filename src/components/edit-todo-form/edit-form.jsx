import React from "react";
import {Button, Form, Icon, Input} from "antd";


function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}


class EditForm extends React.Component {
    state = {
        title:'',
        description:''
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]:e.target.value
        })
    }

    handleUpdate = (e) => {
        e.preventDefault()

        this.props.updateToDoById(this.props.data._id,this.state)
        this.props.onCancel()
    }

    componentDidMount() {
        const { title, description } = this.props.data;
        this.setState({ title, description })
        this.props.form.validateFields();

    }

    render() {
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 19,
                },
            },
        };

        const { getFieldDecorator, getFieldsError } = this.props.form;
        return (
            <Form className="login-form">
                <Form.Item>
                    {getFieldDecorator('title', {
                        initialValue: this.props.data.title,
                        rules: [{ required: true, message: 'Please input title of ToDo' }],
                    })(
                        <Input
                            name = "title"
                            prefix={<Icon type="tag" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            onChange={this.handleChange}
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('description', {
                        initialValue: this.props.data.description,
                        rules: [{ required: true, message: 'Please input description' }],
                    })(
                        <Input
                            name = "description"
                            prefix={<Icon type="rocket" style={{ color: 'rgba(0,0,0,.25)'}} />}
                            onChange={this.handleChange}
                        />,
                    )}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button
                        type="primary"
                        htmlType="submit"
                        onClick={this.handleUpdate}
                        disabled={hasErrors(getFieldsError())}>
                        Change to do
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

export default Form.create({ name: 'add_modal' })(EditForm);