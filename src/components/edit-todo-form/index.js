import ChangeForm from "./edit-form";
import {connect} from 'react-redux'
import actions from '../../store/actions';

const mapStateToProps = state => ({
    toDos: state.toDos
})

const mapDispatchToProps = dispatch => ({
    updateToDoById: (id,data) => dispatch(actions.toDos.updateToDoById(id,data)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChangeForm)