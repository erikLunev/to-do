import AddForm from './add-form';
import {connect} from 'react-redux';
import actions from '../../store/actions';


const mapDispatchToProps = dispatch => ({
    createToDo: data => dispatch(actions.toDos.createToDo(data))
});



export default connect(null, mapDispatchToProps)(AddForm);
