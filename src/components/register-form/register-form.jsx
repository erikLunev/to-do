import React,{Component} from "react";
import { Form, Icon, Input, Button } from 'antd';



function hasErrors(fieldsError) {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
}



class RegisterForm extends Component {

    state = {
        userName:"",
        email:"",
        password:""
    }



    handleRegister = (e) => {
        e.preventDefault()
        this.props.userPostFetch(this.state)
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]:e.target.value
        })
    }

    componentDidMount() {
        this.props.form.validateFields();
    }

    render() {
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 19,
                },
            },
        };
        const { getFieldDecorator,getFieldsError } = this.props.form;
        return (
            <Form className="register-form">
                <Form.Item>
                    {getFieldDecorator('email', {
                        rules: [
                            {
                                type: 'email',
                                message: 'The input is not valid E-mail!',
                            },
                            {
                                required: true,
                                message: 'Please input your E-mail!',
                            },
                        ],
                    })(
                        <Input
                            name = 'email'
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="e.g.john.doe@mail.eg"
                            onChange={this.handleChange}
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('userName', {
                        rules: [{ required: true, message: 'Please input your username!' }],
                    })(
                        <Input
                            name = 'userName'
                            prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            placeholder="Username"
                            onChange={this.handleChange}
                        />,
                    )}
                </Form.Item>
                <Form.Item>
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: 'Please input your Password!' }],
                    })(
                        <Input
                            name = 'password'
                            prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                            type="password"
                            placeholder="Password"
                            onChange={this.handleChange}
                        />,
                    )}
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button
                        type="primary"
                        htmlType="submit"
                        onClick={this.handleRegister}
                        disabled={hasErrors(getFieldsError())}
                    >
                        Register
                    </Button>
                </Form.Item>
            </Form>
        );
    }
}

export default Form.create({ name: 'login' })(RegisterForm);