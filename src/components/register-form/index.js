import RegisterForm from "./register-form";
import {connect} from 'react-redux';
import userPostFetch from "../../store/actions/user-add";


const mapDispatchToProps = dispatch => ({
    userPostFetch: userInfo => dispatch(userPostFetch(userInfo))
})

export default connect(null, mapDispatchToProps)(RegisterForm);

