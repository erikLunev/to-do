import LoginForm from "./login-form";
import {connect} from 'react-redux';
import userLoginFetch from "../../store/actions/user-login";


const mapDispatchToProps = dispatch => ({
    userLoginFetch: userInfo => dispatch(userLoginFetch(userInfo))
})

export default connect(null, mapDispatchToProps)(LoginForm);

