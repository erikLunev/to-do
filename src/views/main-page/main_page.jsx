import React, { Fragment } from 'react'
import { Button, Modal } from 'antd'
import { Welcome } from './styled'
import logo from './media/logo_mini.png'
import RegisterForm from "../../components/register-form";
import LoginForm from "../../components/login-form";


const btnStyle = {
    maxWidth: 300,
    with: 300
}

export default class Main_page extends React.Component{

    state = {
        visibleReg:false,
        visibleLog:false
    }

    setRegVisible(visibleReg) {
        this.setState({ visibleReg });
    }

    setLogVisible(visibleLog) {
        this.setState({ visibleLog });
    }

    render() {
        return (
            <Fragment>
                <Welcome>
                    <Welcome.Card>
                        <img src={logo} alt='' />
                        <Welcome.ButtonsSection >
                            <Button
                                type='primary'
                                style={btnStyle}
                                onClick={() => this.setRegVisible(true)}
                            >
                                Sign up
                            </Button>
                            <Button
                                type='primary'
                                style={btnStyle}
                                onClick={() => this.setLogVisible(true)}
                            >
                                Sign in
                            </Button>
                        </Welcome.ButtonsSection >
                        <Modal
                            title="Registration"
                            visible={this.state.visibleReg}
                            onCancel={() => this.setRegVisible(false)}
                            footer={null}
                        >
                            <RegisterForm />

                        </Modal>
                        <Modal
                            title="Login"
                            visible={this.state.visibleLog}
                            footer={null}
                            onCancel={() => this.setLogVisible(false)}
                        >
                            <LoginForm/>
                        </Modal>
                    </Welcome.Card>
                </Welcome>
            </Fragment>
        )
    }
}
