import ToDoList from "./to-do-list";
import {connect} from 'react-redux'
import actions from '../../store/actions';

const mapStateToProps = state => ({
    toDos: state.toDos
})

const mapDispatchToProps = dispatch => ({
    getToDos: () => dispatch(actions.toDos.getToDos()),
    removeToDoById:(id) => dispatch(actions.toDos.removeToDoById(id)),
    updateToDoById: (id,data) => dispatch(actions.toDos.updateToDoById(id,data))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ToDoList)