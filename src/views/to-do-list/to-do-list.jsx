import React from "react";
import ToDoTable from "./styled";
import {Button,Modal,Divider, Icon, Popconfirm, Checkbox, Spin} from "antd";
import AddForm from "../../components/add-todo-form";
import EditForm from "../../components/edit-todo-form";


export default class ToDoList extends React.Component {
    state = {
        _id: "",
        done: false,
        title: "",
        description: "",
        visibleAdd:false,
        visibleUpd: false,
        currentEditToDo: null
    }

    handleDelete = (id) => {
        this.props.removeToDoById(id)
    }

    handleDone = (todo) => {
        const {_id, done } = todo;
        this.props.updateToDoById(_id, { done: !done })
    }

    setAddVisible(visibleAdd) {
        this.setState({ visibleAdd });
    }

    setUpdVisible(visibleUpd) {
        this.setState({ visibleUpd });
    }

    componentDidMount() {
        return this.props.getToDos()
    }

    setCurrentEditToDO(currentEditToDo) {
        this.setState({currentEditToDo})
    }


    render() {
        const {
            props: {
                toDos
            }
        } = this;

        const columns = [
            {
                title:'Done',
                dataIndex:'done',
                key:'done',
                width: 15,
                render: (value,record) => <Checkbox defaultChecked={value} onChange={() => this.handleDone(record)}></Checkbox>
            },
            {
                title: 'Title',
                dataIndex: 'title',
                key: 'title',
                render: text => <a>{text}</a>,
                width: 30,

            },
            {
                title: 'Description',
                dataIndex: 'description',
                key: 'description',
                width: 150,
            },
            {
                title:'Last updated',
                dataIndex:'updated_at',
                key:'updated_at',
                width:30,
                render: date => (new Date(date)).toDateString(),
                sorter: (a, b) => {
                    const firstDate = new Date(a.updated_at);
                    const secondDate = new Date(b.updated_at);
                    return firstDate - secondDate;
                },
                defaultSortOrder: 'descend'
            },
            {
                title: 'Action',
                key: 'action',
                width: 30,
                render: (value, record) => (
                    <span>
                    <a onClick={() => { this.setCurrentEditToDO(record); this.setUpdVisible(true); }} > Edit</a>
                    <Divider type="vertical" />
                    <Popconfirm title="Sure to delete?" onConfirm={() => this.handleDelete(record._id)}>
                        <a>Delete</a>
                    </Popconfirm>
                  </span>
                ),
            },
        ];
        return (
            <div>
                <Spin spinning={this.props.toDos.isFetching}>
                    <Button onClick={() => this.setAddVisible(true)} type="primary" style={{ marginBottom: 16, marginTop: 16, backgroundColor:"#313483"}}>
                        Add todo
                    </Button>
                    <Modal title="Add todo"  footer={null} visible={this.state.visibleAdd} onCancel={() => this.setAddVisible(false)}>
                        <AddForm onCancel={() => this.setAddVisible(false)}/>
                    </Modal>
                    {toDos.list.length ? (
                        <ToDoTable
                            columns={columns}
                            dataSource={toDos.list}
                            rowKey="_id"
                        />
                    ) : 'No data'}
                    <Modal title="Edit todo" footer={null} visible = {this.state.visibleUpd} onCancel={() => this.setUpdVisible(false)}>
                        <EditForm data={this.state.currentEditToDo} onCancel={() => this.setUpdVisible(false)}/>
                    </Modal>
                </Spin>
            </div>
        )
    }

}
