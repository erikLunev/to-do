import Header from "./header";
import {connect} from 'react-redux'

const mapStateToProps = state => ({
    auth: state.auth
})

const mapDispatchToProps = dispatch => ({
    logOut: () => dispatch({
        type: "LOGIN_USER",
        payload: false
    }),
    logIn: () => dispatch({
        type: "LOGIN_USER",
        payload: true
    })
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Header)