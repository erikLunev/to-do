import React from 'react'
import { HeaderBlock } from './styled'

export default class extends React.Component {

    handleLogOut = (event) => {
        event.preventDefault()
        localStorage.removeItem("token")
        this.props.logOut()
        window.location.href = '/'
    }

    componentDidMount() {
        if (localStorage.getItem('token')) {
            this.props.logIn()
        }
    }

    render() {
        return (
            <HeaderBlock>
                <HeaderBlock.Row>
                    <HeaderBlock.logo>ToDo App</HeaderBlock.logo>
                      {this.props.auth.isLogged ? <HeaderBlock.Logout onClick = {this.handleLogOut} type=' primary' ghost>Log out</HeaderBlock.Logout> : null}
                </HeaderBlock.Row>
            </HeaderBlock>
        )
    }
}
