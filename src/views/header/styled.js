import styled from 'styled-components'
import {Button} from "antd"

const HeaderBlock = styled.header`
  top: 0;
  left: 0;
  width: 100%;
  height: 7rem;
  background: #313483;
  z-index: 15;
  border-bottom: .1rem solid #313483;
`

HeaderBlock.Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 100%;
`

HeaderBlock.logo = styled.h1`
  width: 100%;
  max-width: 20rem;
  margin-left: 2rem;
`

HeaderBlock.Logout = styled(Button)`
  margin-right: 15px;
`

export {HeaderBlock}